import log4j from 'log4js'
import { DeploymentStrategy } from './deploymentStrategy'
import loadConfigFile from './configLoader'

const logger = log4j.getLogger('deploy')

export default async function (
  cwd: string,
  strategy: DeploymentStrategy,
  configFilePath: string,
  args: string[]
) {
  logger.info(`Loading configuration file: ${configFilePath}`)

  const configJson = loadConfigFile(configFilePath)

  logger.debug('Config file contents: ', configJson)

  logger.info(`Creating deployment strategy orchestrator`)
  const orchestrator = strategy.createOrchestrator(configJson, args)

  logger.info(`Executing deployment strategy`)
  await orchestrator.deploy()
}
