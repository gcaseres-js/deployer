export interface IDeploymentTask {
  readonly stage: string
  readonly name: string
  handler(): Promise<any> | any
}

export abstract class DeploymentTask implements IDeploymentTask {
  public readonly stage: string
  public readonly name: string

  public constructor(name: string, stage: string) {
    this.name = name
    this.stage = stage
  }

  public abstract handler(): Promise<any> | any
}
