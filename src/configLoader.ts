import fs from 'fs-extra'

export default function (configFilePath: string): object {
  try {
    return JSON.parse(fs.readFileSync(configFilePath, 'utf8'))
  } catch (e) {
    throw new Error('Error while loading configuration file', {
      cause: e,
    })
  }
}
