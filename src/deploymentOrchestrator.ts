import Orchestrator from 'orchestrator'
import { IDeploymentTask } from './deploymentTask'
import { getLogger } from 'log4js'

class DeploymentStages {
  private stagesMap: { [name: string]: DeploymentStage }
  private orderedStages: string[]

  public constructor() {
    this.stagesMap = {}
    this.orderedStages = []
  }

  public register(stage: DeploymentStage, beforeStageName?: string) {
    this.stagesMap[stage.name] = stage
    if (!beforeStageName) {
      this.orderedStages.push(stage.name)
    } else {
      this.orderedStages.splice(
        this.orderedStages.findIndex((v) => v === beforeStageName),
        0,
        stage.name
      )
    }
  }

  public get(name: string): DeploymentStage {
    if (this.stagesMap[name] === undefined) {
      throw Error(`The stage '${name}' is not registered`)
    }
    return this.stagesMap[name]
  }

  public getAll(): DeploymentStage[] {
    return this.orderedStages.map((name) => this.stagesMap[name])
  }
}

class DeploymentStage {
  public readonly name: string
  private taskMap: { [name: string]: IDeploymentTask }
  private taskDependencies: { [name: string]: string[] }
  private taskNames: string[]

  public constructor(name: string) {
    this.name = name
    this.taskMap = {}
    this.taskDependencies = {}
    this.taskNames = []
  }

  public registerTask(task: IDeploymentTask, dependencyTaskNames: string[]) {
    this.taskMap[task.name] = task
    this.taskDependencies[task.name] = []
    this.taskNames.push(task.name)

    for (let dependencyTaskName of dependencyTaskNames) {
      this.registerDependency(task.name, dependencyTaskName)
    }
  }

  public registerDependency(taskName: string, dependencyTaskName: string) {
    if (!this.taskMap[dependencyTaskName]) {
      throw new Error(
        `The task '${dependencyTaskName}' needs to be registered in the stage '${this.name}' to be declared as dependency`
      )
    }
    this.taskDependencies[taskName].push(dependencyTaskName)
  }

  public getTasks(): IDeploymentTask[] {
    return this.taskNames.map((name) => this.taskMap[name])
  }

  public getTaskDependencies(taskName: string): IDeploymentTask[] {
    return this.taskDependencies[taskName].map((name) => this.taskMap[name])
  }
}

export class DeploymentOrchestrator {
  private static logger = getLogger('DeploymentOrchestrator')

  private stages: DeploymentStages

  public constructor() {
    this.stages = new DeploymentStages()
  }

  public registerStage(
    stageName: string,
    beforeStageName?: string
  ): DeploymentOrchestrator {
    this.stages.register(new DeploymentStage(stageName), beforeStageName)
    return this
  }

  public registerTask(
    task: IDeploymentTask,
    dependencyTaskNames?: string[]
  ): DeploymentOrchestrator {
    this.stages.get(task.stage).registerTask(task, dependencyTaskNames || [])
    return this
  }

  public registerTaskDependency(
    stageName: string,
    taskName: string,
    dependencyTaskName: string
  ): DeploymentOrchestrator {
    this.stages.get(stageName).registerDependency(taskName, dependencyTaskName)
    return this
  }

  private createTaskOrchestrator(
    orderedStages: DeploymentStage[]
  ): Orchestrator {
    const orchestrator = new Orchestrator()

    for (let stageIndex = 0; stageIndex < orderedStages.length; stageIndex++) {
      const stage = orderedStages[stageIndex]

      for (let task of stage.getTasks()) {
        let taskDependencies = stage
          .getTaskDependencies(task.name)
          .map((t) => `${stage.name}_${t.name}`)

        if (stageIndex > 0) {
          taskDependencies.push(orderedStages[stageIndex - 1].name)
        }

        orchestrator.add(
          `${stage.name}_${task.name}`,
          taskDependencies,
          async () => {
            await task.handler()
          }
        )
      }

      let stageDependencies = stage
        .getTasks()
        .map((t) => `${stage.name}_${t.name}`)

      if (stageIndex > 0) {
        stageDependencies.push(orderedStages[stageIndex - 1].name)
      }
      orchestrator.add(stage.name, stageDependencies, () => { })
    }

    return orchestrator
  }

  public async deploy() {
    const orderedStages = this.stages.getAll()
    const orchestrator = this.createTaskOrchestrator(orderedStages)

    return new Promise<void>((resolve, reject) => {
      if (orderedStages.length === 0) {
        DeploymentOrchestrator.logger.info(
          'There are no stages registered in the DeploymentOrchestrator'
        )
        resolve()
      }
      orchestrator.start(
        orderedStages[orderedStages.length - 1].name,
        (error?: any) => {
          if (!error) {
            resolve()
          } else {
            reject(error)
          }
        }
      )
    })
  }
}
