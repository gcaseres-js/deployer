import { DeploymentOrchestrator } from './deploymentOrchestrator'

export interface DeploymentStrategyArgument {
  name: string
  description: string
}

export interface DeploymentStrategy {
  createOrchestrator: (
    deploymentConfig: any,
    deploymentArgs: string[]
  ) => DeploymentOrchestrator
  readonly arguments: DeploymentStrategyArgument[]
}
