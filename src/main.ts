import { Command } from 'commander'
import deploy from './deploy'
import log4j, { getLogger } from 'log4js'
import { DeploymentStrategy } from './deploymentStrategy'

const logger = getLogger('main')

function configureLogger(level: string) {
  log4j.configure({
    appenders: {
      console: { type: 'console' },
    },
    categories: {
      default: { appenders: ['console'], level: level },
    },
  })
}

function createDeployCommand(
  program: Command,
  strategyName: string,
  strategy: DeploymentStrategy
): Command {
  const command = new Command(`deploy-${strategyName.toLowerCase()}`)
    .description(`Initiates deployment script with ${strategyName} strategy`)
    .argument('configFilePath', 'Full path to deployment configuration file')

  for (let strategyArgument of strategy.arguments) {
    command.argument(strategyArgument.name, strategyArgument.description)
  }

  return command.usage('<script> -- [args...]').action(async (...args) => {
    try {
      const options = program.opts()
      configureLogger(options.debug ? 'debug' : 'info')
      await deploy(
        process.cwd(),
        strategy,
        args[0],
        strategy.arguments.map((_v, i) => args[i + 1])
      )
    } catch (e: any) {
      if (e.stdout) {
        logger.info(`Error stdout: ${e.stdout.toString()}`)
      }
      if (e.stderr) {
        logger.info(`Error stderr: ${e.stderr.toString()}`)
      }
      logger.error(e)
      program.error(`Unrecoverable error: ${e}`, { exitCode: 1 })
    }
  })
}

export default async function (
  strategies: { [strategyName: string]: DeploymentStrategy },
  argv: string[]
) {
  const program = new Command('main')
  program.option('-d, --debug', 'output extra debugging')

  for (let strategyName in strategies) {
    program.addCommand(
      createDeployCommand(program, strategyName, strategies[strategyName])
    )
  }

  await program.parseAsync(argv)
}
