module.exports = {
  extends: [
    'plugin:@typescript-eslint/recommended',
    'plugin:prettier/recommended',
  ],
  plugins: ['prettier'],
  parserOptions: {
    parser: '@typescript-eslint/parser',
  },
  env: {
    commonjs: true,
  },
  rules: {
    'prettier/prettier': 2,
  },
}
