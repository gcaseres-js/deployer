import { DeploymentOrchestrator } from '@/deploymentOrchestrator'

describe('@gcaseres-js/deployer/deploymentOrchestrator/DeploymentOrchestrator', () => {
  it('executes registered tasks when calling deploy', () => {
    const orchestrator = new DeploymentOrchestrator()

    orchestrator.registerStage('my-stage')

    const mockedTaskFunction = jest.fn()
    orchestrator.registerTask({
      stage: 'my-stage',
      name: 'my-task',
      handler: mockedTaskFunction,
    })

    orchestrator.deploy()

    expect(mockedTaskFunction).toHaveBeenCalled()
  })

  it('executes tasks with async functions in proper order', async () => {
    const orchestrator = new DeploymentOrchestrator()

    const orderedResults: string[] = []

    orchestrator
      .registerStage('my-stage')
      .registerTask({
        stage: 'my-stage',
        name: 'my-task-1',
        handler: async () => {
          return new Promise<void>((resolve) => {
            const timer = setTimeout(() => {
              orderedResults.push('my-task-1')
              resolve()
            }, 1)
          })
        },
      })
      .registerTask({
        stage: 'my-stage',
        name: 'my-task-2',
        handler: async () => {
          orderedResults.push('my-task-2')
        },
      })
      .registerTaskDependency('my-stage', 'my-task-2', 'my-task-1')

    await orchestrator.deploy()

    expect(orderedResults).toEqual(['my-task-1', 'my-task-2'])
  })

  it('executes tasks and stages in specified order', async () => {
    const orchestrator = new DeploymentOrchestrator()

    const orderedResults: string[] = []

    orchestrator
      .registerStage('my-stage-1')
      .registerStage('my-stage-2')
      .registerTask({
        stage: 'my-stage-1',
        name: 'my-task-1',
        handler: async () => {
          return new Promise<void>((resolve) => {
            const timer = setTimeout(() => {
              orderedResults.push('my-task-1')
              resolve()
            }, 1)
          })
        },
      })
      .registerTask(
        {
          stage: 'my-stage-1',
          name: 'my-task-2',
          handler: () => {
            orderedResults.push('my-task-2')
          },
        },
        ['my-task-1']
      )
      .registerTask({
        stage: 'my-stage-2',
        name: 'my-task-3',
        handler: () => {
          orderedResults.push('my-task-3')
        },
      })

    await orchestrator.deploy()

    expect(orderedResults).toEqual(['my-task-1', 'my-task-2', 'my-task-3'])
  })
})
