import main from '@/main'
import * as deploy from '@/deploy'
import * as configLoader from '@/configLoader'
import { DeploymentStrategy } from '@/deploymentStrategy'
import { DeploymentOrchestrator } from '@/deploymentOrchestrator'
import path from 'path'
import log4j from 'log4js'

jest.mock('log4js', () => {
  const mockedLogger = {
    info: jest.fn(),
    debug: jest.fn(),
    error: jest.fn(),
  };
  return {
    getLogger: jest.fn(() => mockedLogger),
    configure: jest.fn(),
    mockedLogger, // Export for assertions
  };
});

beforeEach(() => {
  jest.restoreAllMocks()
})

describe('@gcaseres-js/deployer/main', () => {
  it('executes the deployment strategy as expected', async () => {
    const executionState: string[] = []

    const strategy: DeploymentStrategy = {
      arguments: [
        {
          name: 'arg-1',
          description: '',
        },
        {
          name: 'arg-2',
          description: '',
        },
      ],
      createOrchestrator: (
        deploymentConfig: { key: string },
        deploymentArgs: string[]
      ) => {
        const orchestrator = new DeploymentOrchestrator()

        orchestrator.registerStage('stage-1').registerTask({
          stage: 'stage-1',
          name: 'task-1',
          handler: () => {
            executionState.push(deploymentConfig.key)
            executionState.push(deploymentArgs[0])
            executionState.push(deploymentArgs[1])
          },
        })

        return orchestrator
      },
    }

    await main({ mystrategy: strategy }, [
      'node',
      'deployer',
      'deploy-mystrategy',
      path.resolve(__dirname, '../fixtures/config/dummy.json'),
      'arg-value-1',
      'arg-value-2',
    ])

    expect(executionState[0]).toEqual('value')
    expect(executionState[1]).toEqual('arg-value-1')
    expect(executionState[2]).toEqual('arg-value-2')
  })

  it('supports multiple deployment strategies', async () => {
    const executionState: string[] = []

    const strategy1: DeploymentStrategy = {
      arguments: [
        {
          name: 'arg-1',
          description: '',
        },
        {
          name: 'arg-2',
          description: '',
        },
      ],
      createOrchestrator: (
        deploymentConfig: { key: string },
        deploymentArgs: string[]
      ) => {
        const orchestrator = new DeploymentOrchestrator()

        orchestrator.registerStage('stage-1').registerTask({
          stage: 'stage-1',
          name: 'task-1',
          handler: () => {
            executionState.push(deploymentConfig.key)
            executionState.push(deploymentArgs[0])
            executionState.push(deploymentArgs[1])
          },
        })

        return orchestrator
      },
    }

    const strategy2: DeploymentStrategy = {
      arguments: [
        {
          name: 'arg-1',
          description: '',
        }
      ],
      createOrchestrator: (
        deploymentConfig: { key: string },
        deploymentArgs: string[]
      ) => {
        const orchestrator = new DeploymentOrchestrator()

        orchestrator.registerStage('stage-1').registerTask({
          stage: 'stage-1',
          name: 'task-1',
          handler: () => {
            executionState.push(deploymentConfig.key)
            executionState.push(deploymentArgs[0])
          },
        })

        return orchestrator
      },
    }

    await main({ strategy1: strategy1, strategy2: strategy2 }, [
      'node',
      'deployer',
      'deploy-strategy1',
      path.resolve(__dirname, '../fixtures/config/dummy.json'),
      'arg-value-1',
      'arg-value-2',
    ])

    expect(executionState[0]).toEqual('value')
    expect(executionState[1]).toEqual('arg-value-1')
    expect(executionState[2]).toEqual('arg-value-2')

    await main({ strategy1: strategy1, strategy2: strategy2 }, [
      'node',
      'deployer',
      'deploy-strategy2',
      path.resolve(__dirname, '../fixtures/config/dummy.json'),
      'arg-value-1',
    ])

    expect(executionState[3]).toEqual('value')
    expect(executionState[4]).toEqual('arg-value-1')

  })

  it('calls deploy function with expected arguments', async () => {
    jest.spyOn(configLoader, 'default').mockImplementation(jest.fn())
    const deploySpy = jest
      .spyOn(deploy, 'default')
      .mockImplementation(jest.fn())

    const strategy: DeploymentStrategy = {
      arguments: [
        {
          name: 'arg-1',
          description: '',
        },
        {
          name: 'arg-2',
          description: '',
        },
      ],
      createOrchestrator: jest.fn(),
    }

    await main({ mystrategy: strategy }, [
      'node',
      'deployer',
      'deploy-mystrategy',
      'config-path',
      'arg-1',
      'arg-2',
    ])

    expect(deploySpy).toHaveBeenCalledWith(
      expect.any(String),
      strategy,
      'config-path',
      ['arg-1', 'arg-2']
    )
  })

  it('returns an error exit code if something fails when executing deploy strategy', async () => {
    jest.spyOn(configLoader, 'default').mockImplementation(jest.fn())
    const deploySpy = jest.spyOn(deploy, 'default')

    const orchestrator = new DeploymentOrchestrator()
    orchestrator.deploy = async () => {
      throw new Error()
    }

    const strategy: DeploymentStrategy = {
      arguments: [
        {
          name: 'arg-1',
          description: '',
        },
        {
          name: 'arg-2',
          description: '',
        },
      ],
      createOrchestrator: () => orchestrator,
    }

    const processExitSpy = jest
      .spyOn(process, 'exit')
      .mockImplementation((code) => {
        return undefined as never
      })

    await main({ mystrategy: strategy }, [
      'node',
      'deployer',
      'deploy-mystrategy',
      'config-path',
      'arg-1',
      'arg-2',
    ])

    expect(deploySpy).toHaveBeenCalledWith(
      expect.any(String),
      strategy,
      'config-path',
      ['arg-1', 'arg-2']
    )

    expect(processExitSpy).toHaveBeenCalled()
  })

  it('logs error information if something fails when executing deploy strategy', async () => {
    jest.spyOn(configLoader, 'default').mockImplementation(jest.fn())

    const orchestrator = new DeploymentOrchestrator()
    orchestrator.deploy = async () => {
      throw {
        stdout: Buffer.from('stdout message'),
        stderr: 'stderr message',
      }
    }

    const strategy: DeploymentStrategy = {
      arguments: [],
      createOrchestrator: () => orchestrator,
    }

    const processExitSpy = jest
      .spyOn(process, 'exit')
      .mockImplementation((code) => {
        return undefined as never
      })

    const mockedLogger = log4j.getLogger()

    await main({ mystrategy: strategy }, [
      'node',
      'deployer',
      'deploy-mystrategy',
      'config-path'
    ])

    expect(mockedLogger.info).toHaveBeenCalledWith('Error stdout: stdout message')
    expect(mockedLogger.info).toHaveBeenCalledWith('Error stderr: stderr message')
  })

  it('returns an error exit code if something fails when trying to load/parse config file', async () => {
    jest.spyOn(configLoader, 'default').mockImplementation(() => {
      throw Error('An expected error')
    })
    const deploySpy = jest.spyOn(deploy, 'default')

    const strategy: DeploymentStrategy = {
      arguments: [
        {
          name: 'arg-1',
          description: '',
        },
        {
          name: 'arg-2',
          description: '',
        },
      ],
      createOrchestrator: jest.fn(),
    }

    const processExitSpy = jest
      .spyOn(process, 'exit')
      .mockImplementation((code) => {
        return undefined as never
      })

    await main({ mystrategy: strategy }, [
      'node',
      'deployer',
      'deploy-mystrategy',
      'config-path',
      'arg-1',
      'arg-2',
    ])

    expect(deploySpy).toHaveBeenCalledWith(
      expect.any(String),
      strategy,
      'config-path',
      ['arg-1', 'arg-2']
    )

    expect(processExitSpy).toHaveBeenCalled()
  })
})
