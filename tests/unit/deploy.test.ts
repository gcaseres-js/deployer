import deploy from '@/deploy'
import Orchestrator from 'orchestrator'
import path from 'path'
import { DeploymentStrategy } from '@/deploymentStrategy.js'
import { DeploymentOrchestrator } from '@/deploymentOrchestrator'

describe('@gcaseres-js/deployer/deploy', () => {
  it('calls the strategy module with expected parameters', async () => {
    const strategy: DeploymentStrategy = {
      arguments: [
        {
          name: 'arg-1',
          description: '',
        },
        {
          name: 'arg-2',
          description: '',
        },
      ],
      createOrchestrator: () => new DeploymentOrchestrator(),
    }

    const createOrchestratorSpy = jest.spyOn(strategy, 'createOrchestrator')

    await deploy(
      'cwd',
      strategy,
      path.resolve(__dirname, '../fixtures/config/dummy.json'),
      ['arg-1', 'arg-2']
    )

    expect(createOrchestratorSpy).toHaveBeenCalledWith({ key: 'value' }, [
      'arg-1',
      'arg-2',
    ])
  })
})
